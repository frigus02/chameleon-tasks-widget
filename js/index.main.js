$(document).ready(function() {
    var REFRESH_TIMESPAN = 60000 * 10; // Every 10 minutes
    var lastRefresh = null;
    var reLink = /((http|https|ftp)\:\/\/([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(\/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*\/?)/g;
    
    chameleon.widget({
        //Triggered every time the widget loads.
        onLoad: function() {
            chameleon.localization.update();
            tasks.ALL_LIST.title = _('tasks_tasklist_all_title');
            chameleon.action({
                title: _('tasks_action_new'),
                icon_up: 'images/action_new.png',
                icon_down: 'images/action_new_down.png'
            });

            updateTitle();
            updateStyle();
            startPolling();
        },
        
        //Triggered everytime Chameleon resumes (comes back into focus).
        onResume: function() {
            chameleon.localization.update();
            tasks.ALL_LIST.title = _('tasks_tasklist_all_title');
            chameleon.action({
                title: _('tasks_action_new'),
                icon_up: 'images/action_new.png',
                icon_down: 'images/action_new_down.png'
            });
            
            startPolling();
        },
        
        //Triggered every time Chameleon pauses (goes out of focus).
        onPause: function() {
            stopPolling();
        },
        
        //Triggered when the user enters dashboard edit mode.
        onLayoutModeStart: function() {
            stopPolling();
        },
        
        //Triggered when the user exits dashboard edit mode.
        onLayoutModeComplete: function() {
            startPolling();
        },
        
        //Triggered when the status of network availability changes.
        onConnectionAvailableChanged: function(available) {
            if(available) {
                startPolling();
            } else {
                stopPolling();
            }
        },
        
        //Triggered when the user taps the configure button in the widget title bar.
        onConfigure: function() {
            showSettings();
        },
        
        //Triggered when the user taps the widget titlebar.
        onTitleBar: function() {
            launchTasksApp();
        },
        
        //Triggered when the user taps the action button on the widget title bar.
        onAction: function() {
            newTask();
        },

        //Triggered when the user taps the refresh button on the widget title bar.
        onRefresh: function() {
            updateData();
        },
        
        //Triggered every time the widget loads, but not in Chameleon.                
        notChameleon: function() {
            $('#chameleon-widget').css('background-color', '#888');
            updateView();
        }
    });
    
    //----------------------------------------------------------------------
    // Event handlers
    //----------------------------------------------------------------------
    $('*').live('touchstart', function(e) {
        $(this).addClass('active');
        if($(this).is('.task .actions li')) {
            e.stopPropagation();
        }
    }).live('touchend touchmove touchcancel', function() {
        $(this).removeClass('active');
    });
    
    $('#tasklists .tasklist:first-child').live('click', function() {
        $('#tasklists .tasklist:not(:first-child)').toggle();
    });
    
    $('#tasklists .tasklist:not(:first-child)').live('click', function() {
        tasks.setCurrentList({
            id: $(this).data('id'),
            title: $(this).text()
        });
        if(tasks.areTasksLoaded()) {
            updateView();
        } else {
            chameleon.showLoading({ showloader: true });
            tasks.load(function(success) {
                chameleon.showLoading({ showloader: false });
                updateView();
            });
            lastRefresh = new Date().getTime();
        }
    });
    
    $('#tasks .task').live('click', function() {
        $(this).children('.actions').toggle()
        $(this).siblings().children('.actions').hide();
    });
    
    $('#tasks .task .actions .edit').live('click', function() {
        var task = $(this).parents('.task').data('task');
        editTask(task);
    });

    $('#tasks .task .actions .markAsCompleted').live('click', function() {
        var task = $(this).parents('.task').data('task');
        task.markAsCompleted();
        updateView();
    });
    
    //----------------------------------------------------------------------
    // Functions
    //----------------------------------------------------------------------
    function updateData() {
        if(!chameleon.connected()) {
            if(lastRefresh == null) {
                $('#chameleon-widget').chameleonWidgetNeedWiFiHTML();
                chameleon.invalidate();
            } else {
                // Maybe show small message at the bottom.
            }
        } else if(!gapi.getAccount() || !tasks.getCurrentList()) {
            $('#chameleon-widget').chameleonWidgetConfigureHTML({
                onConfigure: showSettings,
                title: _('tasks_configure_prompt_title'),
                caption: _('tasks_configure_prompt_caption')
            });
            chameleon.invalidate();
        } else {
            chameleon.showLoading({ showloader: true });
            tasks.load(function(success) {
                chameleon.showLoading({ showloader: false });
                if(success) {
                    updateView();
                }
            });
            lastRefresh = new Date().getTime();
        }
    }
    
    function updateTitle() {
        var account = gapi.getAccount();
        var tasklist = tasks.getCurrentList();
        if(account && tasklist) {
            if(style.isShowAccountInTitleBar() && !style.isShowTasklistSelector()) {
                chameleon.setTitle({ text: tasklist.title + ': ' + account.email });
            } else if(!style.isShowTasklistSelector()) {
                chameleon.setTitle({ text: tasklist.title });
            } else {
                chameleon.setTitle({ text: 'Google Tasks' });
            }
        } else {
            chameleon.setTitle({ text: 'Google Tasks' });
        }
    }
    
    function updateStyle() {
        var classes = style.getCssClasses();
        $('#chameleon-widget').attr('class', classes.join(' '));
        $('#custom-style-sheet').attr('href', style.getCustomStyleSheetURL());
    }
    
    function updateView() {
        var list = [];
                
        var currentTasklist = tasks.getCurrentList();
        if(style.isShowTasklistSelector()) {
            list.push('<ul id="tasklists">');
            list.push('<li class="tasklist">', currentTasklist.title, '</li>');
            var tasklists = tasks.getLists();
            for(var i = 0, tasklist; tasklist = tasklists[i]; i++) {
                if(tasklist.id != currentTasklist.id) {
                    list.push('<li class="tasklist" data-id="', tasklist.id, '">', tasklist.title, '</li>');
                }
            }
            list.push('</ul>');
        }
        
        var items = tasks.getTasks();
        if(items.length > 0) {
            list.push('<ul id="tasks">');
            var indentTasks = tasks.getCurrentSort() === 'sortByPosition';
            var indents = {};
            for(var i = 0, item; item = items[i]; i++) {
                var classes = ['task'];
                var indent = 0;
                if(indentTasks) {
                    if(item.parent) {
                        var parentIndent = indents[item.parent] || 0;
                        var indent = parentIndent + 20;
                        indents[item.id] = indent;
                    }
                }
    
                var due = '';
                if(item.due) {
                    var date = new Date(item.due);
                    var today = new Date(new Date().toISOString().substr(0, 10) + 'T00:00:00.000Z');
                    
                    var msPerDay = 86400000; // milliseconds per day
                    if(date.getTime() < today.getTime() - msPerDay) {
                        due = _('tasks_time_daysago', (today.getTime() - date.getTime()) / msPerDay);
                    } else if(date.getTime() == today.getTime() - msPerDay) {
                        due = _('tasks_time_yesterday');
                    } else if(date.getTime() == today.getTime()) {
                        due = _('tasks_time_today');
                    } else if(date.getTime() == today.getTime() + msPerDay) {
                        due = _('tasks_time_tomorrow');
                    } else {
                        due = date.toLocaleShortDateString();
                    }
                    
                    if(date.getTime() == today.getTime()) {
                        classes.push('due');
                    } else if(date < today) {
                        classes.push('overdue');
                    }
                }
                
                var notes = '';
                if(item.notes) {
                    notes = '<p>' + item.notes.replace(/\n\n/g, '</p><p>').replace(/\n/g, '<br>') + '</p>';
                    notes = notes.replace(reLink, '<a href="$1">$1</a>');
                }
                
                var tasklist = '';
                if(currentTasklist.id == tasks.ALL_LIST.id) {
                    tasklist = item.list.title;
                }
                
                list.push(
                    '<li class="', classes.join(' '), '" style="padding-left:', indent, 'px;" data-id="', item.id, '">',
                        '<header>',
                            '<div class="row">',
                                '<span class="title">', item.title, '</span>',
                                '<span class="duedate">', due, '</span>',
                            '</div>',
                            '<div class="row">',
                                '<span class="tasklist">', tasklist, '</span>',
                            '</div>',
                        '</header>',
                        '<div class="notes">', notes, '</div>',
                        '<ul class="actions">',
                            '<li class="action edit">', _('tasks_action_edit'), '</li>',
                            '<li class="action markAsCompleted">', _('tasks_action_mark_as_completed'), '</li>',
                        '</ul>',
                    '</li>');
            }
            list.push('</ul>');
        } else {
            list.push('<div id="no-tasks"></div>');
        }
        
        $('#chameleon-widget').html(list.join('')).chameleonProxyAllLinks();
        chameleon.invalidate();
        
        if(items.length > 0) {
            for(var i = 0, item; item = items[i]; i++) {
                $('.task[data-id="' + item.id + '"]').data('task', item);
            }
        } else {
            $('#no-tasks').chameleonWidgetMessageHTML({
                title: _('tasks_notasks_beer_title'),
                caption: _('tasks_notasks_beer_caption'),
                icon: 'images/beer.png'
            });
            chameleon.invalidate();
        }
    }
    
    function startPolling() {
        if(lastRefresh == null || new Date().getTime() - lastRefresh >= REFRESH_TIMESPAN) {
            updateData();
        }
        
        if(gapi.getAccount() !== undefined) {
            chameleon.poll({
                id: 'refresh_tasks',
                action: 'start',
                interval: REFRESH_TIMESPAN,
                callback: updateData
            });
        }
    }
    
    function stopPolling() {
        chameleon.poll({
            id: 'refresh_tasks',
            action: 'stop'
        });
    }
    
    function launchTasksApp() {    
        // Supported apps
        var apps = [{
            package: 'ch.teamtasks.tasks.paid',
            name: 'ch.teamtasks.tasks.MainActivity'
        }, {
            package: 'ch.teamtasks.tasks',
            name: 'ch.teamtasks.tasks.MainActivity'
        }, {
            package: 'org.dayup.gtask',
            name: 'org.dayup.gtask.activity.TaskActivity'
        }];

        // Try to start a supported app.
        for(var i = 0, app; app = apps[i]; i++) {
            if(chameleon.componentExists(app)) {
                chameleon.intent({
                    component: app,
                    action: 'android.intent.action.MAIN'
                });
                return;
            }
        }
        
        // If no supported app was found, open website.
        chameleon.intent({
            action: 'android.intent.action.VIEW',
            data: 'https://mail.google.com/tasks/android'
        });
    }
    
    function showSettings() {
        stopPolling();
        chameleon.promptHTML({
            url: 'settings.html',
            result: function(success, data) {
                if(success) {
                    updateTitle();
                    updateStyle();
                    lastRefresh = null;
                }
                startPolling();
            }
        });
    }

    function newTask() {
        var tasklist = tasks.getCurrentList();
        var params = {
            action: 'new',
            lists: JSON.stringify(tasks.getLists(true))
        };
        if(tasklist.id != tasks.ALL_LIST.id) {
            params.selectListID = tasklist.id;
        }

        chameleon.promptHTML({
            url: 'task.html?' + $.param(params),
            result: function(success, data) {
                if(success) {
                    data.notes = decodeURIComponent(data.notes);
                    tasklist = data.list;
                    delete data.list;
                    tasks.addTask(tasklist, data);
                    updateView();
                }
            }
        });
    }

    function editTask(task) {
        var params = {
            action: 'edit',
            data: JSON.stringify({
                title: task.title,
                due: task.due ? task.due : '',
                notes: task.notes ? task.notes : ''
            })
        };

        chameleon.promptHTML({
            url: 'task.html?' + $.param(params),
            result: function(success, data) {
                if(success) {
                    data.notes = decodeURIComponent(data.notes);
                    task.edit(data);
                    updateView();
                }
            }
        });
    }
});