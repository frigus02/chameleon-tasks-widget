var style = {};

style.getFontSize = function() {
    var data = chameleon.getData() || {};
    if(data.style && data.style.fontSize) {
        return data.style.fontSize;
    } else {
        return "normal";
    }
};

style.setFontSize = function(fontSize) {
    var data = chameleon.getData() || {};
    data.style = data.style || {};
    data.style.fontSize = fontSize;
    chameleon.saveData(data);
};

style.isShowAccountInTitleBar = function() {
    var data = chameleon.getData() || {};
    if(data.style && typeof data.style.showAccountInTitleBar != 'undefined') {
        return data.style.showAccountInTitleBar;
    } else {
        return true;
    }
};

style.setShowAccountInTitleBar = function(showAccount) {
    var data = chameleon.getData() || {};
    data.style = data.style || {};
    data.style.showAccountInTitleBar = showAccount;
    chameleon.saveData(data);
};

style.isShowTasklistSelector = function() {
    var data = chameleon.getData() || {};
    return data.style && data.style.showTasklistSelector;
};

style.setShowTasklistSelector = function(showSelector) {
    var data = chameleon.getData() || {};
    data.style = data.style || {};
    data.style.showTasklistSelector = showSelector;
    chameleon.saveData(data);
};

style.getCustomStyleSheetURL = function() {
    var data = chameleon.getData() || {};
    return data.style && data.style.customStyleSheetURL;
};

style.setCustomStyleSheetURL = function(url) {
    var data = chameleon.getData() || {};
    data.style = data.style || {};
    data.style.customStyleSheetURL = url;
    chameleon.saveData(data);
};

style.getCssClasses = function() {
    return [
        'font-' + style.getFontSize()
    ];
};