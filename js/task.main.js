$(document).ready(function() {
    chameleon.localization.update();
    populateStaticTextForLocale();

	$('#due').mobiscroll().date({
        theme: 'android-ics',
        display: 'modal',
        mode: 'scroller',
        endYear: new Date().getFullYear() + 100,
        showNow: true,
        dateOrder: Date.getLocaleShortDateFormat().replace(/MM/, 'M').replace(/[\.\/]/g, ''),
        dateFormat: Date.getLocaleShortDateFormat().toLowerCase()
    });

	$('#clear-due-button').click(function(e) {
		e.preventDefault();
		$('#due').val('');
	});

	$('#save-button').click(function(e) {    
        e.preventDefault();
        var data = {
        	title: $('#title').val(),
        	due: $('#due').val() ? $('#due').mobiscroll('getDate').toISOString() : null,
        	// There seems to be an issue in chameleon, when notes contains a new line.
        	// > Uncaught SyntaxError: Unexpected token
        	// >  at http://192.168.178.42/chameleon/tasks/:1
        	// In order to work around this, we use encodeURIComponent.
        	notes: encodeURIComponent($('#notes').val())
        };

        if($('#completed').prop('checked')) {
        	data.status = 'completed';
        }
        
        if($('#tasklist-fields').is(":visible")) {
            var selectedTasklist = $('#tasklist-select').chameleonSelectList({ getSelectedItem: true });
            data.list = {
                title: selectedTasklist.name,
                id: selectedTasklist.value
            };
        }
        
        chameleon.close(true, data);
    });

	var action = getURLParameter('action');
	if(action == 'edit') {
		var data = JSON.parse(getURLParameter('data'));
		$('#chameleon-widget-settings > header')._('tasks_task_header_edit');
	    $('#title').val(data.title);
		if(data.due) {
			$('#due').mobiscroll('setDate', new Date(data.due), true);
		}
		$('#notes').val(data.notes);
	} else {
		$('#chameleon-widget-settings > header')._('tasks_task_header_new');
	}

    var lists = JSON.parse(getURLParameter('lists'));
    var selectListID = getURLParameter('selectListID');
    if(lists) {
        var options = [];
        for(var i = 0, list; list = lists[i]; i++) {
            options.push({
                name: list.title,
                value: list.id
            });
        }
        $('#tasklist-select').chameleonSelectList({
            title: _('tasks_task_tasklist_select_title'),
            list: options
        });

        if(selectListID) {
            $('#tasklist-select').chameleonSelectList({ selectedValue: selectListID });
        }
    } else {
        $('#tasklist-fields').hide();
    }

    function populateStaticTextForLocale() {
        $('#title-fields label')._('tasks_task_title_label');
        $('label[for="completed"]')._('tasks_task_completed_label');
        $('#duedate-fields label')._('tasks_task_duedate_label');
        $('#notes-fields label')._('tasks_task_notes_label');
        $('#tasklist-fields label')._('tasks_task_tasklist_label');
        $('#save-button')._('tasks_task_save');
    }

	function getURLParameter(name) {
	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
	}
});