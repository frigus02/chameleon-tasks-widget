$(document).ready(function() {
    chameleon.localization.update();
    tasks.ALL_LIST.title = _('tasks_tasklist_all_title');
    populateStaticTextForLocale();

    $('.tabs a').click(function(e) {
        e.preventDefault();
        
        $('.tabs a.active').removeClass('active');
        $('.tab-content.active').removeClass('active');
        
        $(this).addClass('active');
        $($(this).attr('href')).addClass('active');
    });
    
    $('#add-account-button').click(function(e) {
        e.preventDefault();
        gapi.addAccount(populateAccounts);
    });
    
    $('#close-button').click(function(e) {    
        e.preventDefault();
        
        var selectedAccount = $('#account-select').chameleonSelectList({ getSelectedItem: true }).value;
        var selectedTasklist = $('#tasklist-select').chameleonSelectList({ getSelectedItem: true });
        var selectedSort = $('#sort-select').chameleonSelectList({ getSelectedItem: true }).value;
        var showAccountName = $('#show-account-name').prop('checked');
        var showTasklistSelector = $('#show-tasklist-selector').prop('checked');
        var selectedFontSize = $('#fontsize-select').chameleonSelectList({ getSelectedItem: true }).value;
        var customStyleSheet = $('#custom-style-sheet').val();
        
        if(selectedAccount) {
            gapi.getAccount(selectedAccount).setAsCurrent();
        }
        tasks.setCurrentList({
            id: selectedTasklist.value,
            title: selectedTasklist.name
        });
        tasks.setCurrentSort(selectedSort);
        style.setShowAccountInTitleBar(showAccountName);
        style.setShowTasklistSelector(showTasklistSelector);
        style.setFontSize(selectedFontSize);
        style.setCustomStyleSheetURL(customStyleSheet);
        chameleon.close(true);
    });
    
    $('#sort-select').chameleonSelectList({
        title: _('tasks_settings_sortby_select_title'),
        list: [{
            name: _('tasks_settings_sortby_select_option_duedate'),
            value: 'sortByDueDate'
        }, {
            name: _('tasks_settings_sortby_select_option_custom'),
            value: 'sortByPosition'
        }]
    });
    
    $('#fontsize-select').chameleonSelectList({
        title: _('tasks_settings_fontsize_select_title'),
        list: [{
            name: _('tasks_settings_fontsize_select_option_xsmall'),
            value: 'xsmall'
        }, {
            name: _('tasks_settings_fontsize_select_option_small'),
            value: 'small'
        }, {
            name: _('tasks_settings_fontsize_select_option_normal'),
            value: 'normal'
        }, {
            name: _('tasks_settings_fontsize_select_option_large'),
            value: 'large'
        }, {
            name: _('tasks_settings_fontsize_select_option_xlarge'),
            value: 'xlarge'
        }]
    });
    
    $('.tabs a:first').click();
    $('#account-select').hide();
    $('#tasklist-fields').hide();
    $('#sort-select').chameleonSelectList({ selectedValue: tasks.getCurrentSort() });
    $('#show-account-name').prop('checked', style.isShowAccountInTitleBar());
    $('#show-tasklist-selector').prop('checked', style.isShowTasklistSelector());
    $('#fontsize-select').chameleonSelectList({ selectedValue: style.getFontSize() });
    $('#custom-style-sheet').val(style.getCustomStyleSheetURL());
    $('#chameleon-widget-settings .help').chameleonProxyAllLinks();
    populateAccounts();
    
    //----------------------------------------------------------------------
    // Functions
    //----------------------------------------------------------------------
    function populateStaticTextForLocale() {
        $('#tab-general')._('tasks_settings_tab_general');
        $('#tab-style')._('tasks_settings_tab_style');

        $('#account-fields label')._('tasks_settings_account_label');
        $('#account-fields .help')._('tasks_settings_account_help');
        $('#add-account-button')._('tasks_settings_account_add_account');

        $('#tasklist-fields label')._('tasks_settings_tasklist_label');

        $('#sortby-fields label')._('tasks_settings_sortby_label');

        $('label[for="show-account-name"]')._('tasks_settings_showaccointintitlebar_label');
        $('label[for="show-tasklist-selector"]')._('tasks_settings_showtasklistselectorinwidget_label');

        $('#fontsize-fields label')._('tasks_settings_fontsize_label');

        $('#customstylesheet-fields label')._('tasks_settings_customstylesheet_label');
        $('#customstylesheet-fields .help')._('tasks_settings_customstylesheet_help');

        $('#close-button')._('tasks_settings_done');
    }

    function populateAccounts(accountToSelect) {
        if(accountToSelect === undefined) {
            accountToSelect = gapi.getAccount();
        }

        var availableAccounts = gapi.getAccounts();
        if(availableAccounts.length > 0) {
            $('#account-select').show();
            var options = [];
            for(var i = 0, account; account = availableAccounts[i]; i++) {
                options.push({
                    name: account.email,
                    value: account.id
                });
            }

            $('#account-select').chameleonSelectList({
                title: _('tasks_settings_account_select_title'),
                list: options,
                changed: function(info) {
                    populateTasklists();
                },
                allow_delete: true,
                deleted: function(items) {
                    for(var i = 0, item; item = items[i]; i++) {
                        gapi.getAccount(item.value).remove();
                    }
                    
                    if(gapi.getAccounts().length == 0) {
                        $('#account-select').hide();
                        $('#tasklist-fields').hide();
                    } else {
                        populateTasklists();
                    }
                }
            });
            
            if(accountToSelect !== undefined) {
                $('#account-select').chameleonSelectList({ selectedValue: accountToSelect.id });
            }
            
            populateTasklists();
        }
    }

    function populateTasklists() {
        var selectedAccount = gapi.getAccount($('#account-select').chameleonSelectList({ getSelectedItem: true }).value);
        if(!selectedAccount) return;
        
        chameleon.showLoading({ showloader: true });
        tasks.getListsForAccount(selectedAccount, function(success, lists) {
            chameleon.showLoading({ showloader: false });
            if(!success) return;
            
            $('#tasklist-fields').show();
            
            var options = [];
            for(var i = 0; i < lists.length; i++) {
                options.push({
                    name: lists[i].title,
                    value: lists[i].id
                });
            }
            $('#tasklist-select').chameleonSelectList({
                title: _('tasks_settings_tasklist_select_title'),
                list: options,
            });
            
            var currentList = tasks.getCurrentList();
            if(currentList !== undefined) {
                $('#tasklist-select').chameleonSelectList({ selectedValue: currentList.id });
            }
        });
    }
});