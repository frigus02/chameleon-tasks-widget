var tasks = (function() {
    // Public
    var me = {};
    
    me.load = function(callback) {
        loadTaskLists(function(success, lists) {
            if(!success) {
                if(callback) callback(false);
                return;
            }
            
            var currentList = me.getCurrentList();
            var now = new Date().getTime();
            var newData = {};
            for(var i = 0, list; list = lists[i]; i++) {
                newData[list.id] = {
                    title: list.title,
                    tasks: [],
                    expires: 0
                };
                
                if(currentList.id == me.ALL_LIST.id || currentList.id == list.id) {
                    // If data for this list will be loaded now
                    newData[list.id].expires = now + (10 * 60000);
                } else if(data[list.id] && data[list.id].expires > now) {
                    // If data for this list will not be loaded now, was loaded before
                    // and is not yet expired
                    newData[list.id].tasks = data[list.id].tasks;
                    newData[list.id].expires = data[list.id].expires;
                }
            }
            data = newData;
            
            if(me.getCurrentList()) {
                loadTasks(function(tasks) {
                    for(var i = 0, task; task = tasks[i]; i++) {
                        data[task.list.id].tasks.push(task);
                    }
                    
                    if(callback) callback(true);
                });
            }
        });
    };
    
    me.getListsForAccount = function(account, callback, noAll) {
        loadTaskLists(function(success, lists) {
            if(success) {
                if(!noAll) {
                    lists.unshift(me.ALL_LIST);
                }
                callback(true, lists);
            } else {
                callback(false);
            }
        }, account);
    };
    
    me.getLists = function(noAll) {
        var lists = [];
        if(!noAll) {
            lists.unshift(me.ALL_LIST);
        }
        for(var id in data) {
            lists.push({
                id: id,
                title: data[id].title
            });
        }
        return lists;
    };
    
    me.getTasks = function() {
        var tasks = [];
        var currentList = me.getCurrentList();
        for(var id in data) {
            if(currentList.id == me.ALL_LIST.id || id == currentList.id) {
                for(var i = 0, task; task = data[id].tasks[i]; i++) {
                    if(task.status != 'completed') {
                        tasks.push(task);
                    }
                }
            }
        }

        var sort = sortMethods[me.getCurrentSort()];
        tasks.sort(sort);

        return tasks;
    };
    
    me.areTasksLoaded = function() {
        var currentList = me.getCurrentList();
        var now = new Date().getTime();
        for(var id in data) {
            if((currentList.id == me.ALL_LIST.id || id == currentList.id)
               && data[id].expires < now) {
                return false;
            }
        }
        return true;
    };

    me.addTask = function(list, values) {
        var task = new Task(values, list);
        data[list.id].tasks.push(task);

        new gapi.Request({
            path: '/tasks/v1/lists/' + list.id + '/tasks',
            method: 'POST',
            params: {
                fields: 'due,id,notes,parent,position,title'
            },
            body: values
        }).execute(function(success, data) {
            if(success) {
                $.extend(task, data);
            }
        });
    };
    
    me.getCurrentList = function() {
        var data = chameleon.getData() || {};
        return data.tasklist;
    };
    
    me.setCurrentList = function(list) {
        var data = chameleon.getData() || {};
        data.tasklist = list;
        chameleon.saveData(data);
    };
    
    me.getCurrentSort = function() {
        var data = chameleon.getData() || {};
        return data.tasksort || 'sortByDueDate';
    };
    
    me.setCurrentSort = function(functionName) {
        if(sortMethods[functionName]) {
            var data = chameleon.getData() || {};
            data.tasksort = functionName;
            chameleon.saveData(data);
        }
    };
    
    me.ALL_LIST = {
        id: 'all',
        title: 'All'
    };


    var data = {};
    var sortMethods = {
        sortByDueDate: function(a, b) {
            if(!a.due && !b.due) return 0;
            if(!a.due) return 1;
            if(!b.due) return -1;
            return a.due < b.due ? -1 : (a.due > b.due ? 1 : 0);
        },
    
        sortByPosition: function(a, b) {
            return a.list.title < b.list.title ? -1 : (a.list.title > b.list.title ? 1 : 0);
        }
    };
    
    function Task(values, list) {
        $.extend(this, values);
        this.list = list;
        
        this.markAsCompleted = function() {
            this.edit({
                status: 'completed'
            });
        };
        
        this.edit = function(newValues) {
            // Change local values for immediate response.
            $.extend(this, newValues);
            
            // Push changes to Google.
            var self = this;
            new gapi.Request({
                path: '/tasks/v1/lists/' + self.list.id + '/tasks/' + self.id,
                method: 'PATCH',
                params: {
                    fields: 'due,id,notes,parent,position,title'
                },
                body: newValues
            }).execute(function(success, data) {
                if(success) {
                    $.extend(self, data);
                }
            });
        };
    }

    function loadTaskLists(callback, account) {
        new gapi.Request({
            path: '/tasks/v1/users/@me/lists',
            method: 'GET',
            params: {
                fields: 'items(id,title)'
            },
            account: account
        }).execute(function(success, data) {
            if(success) {
                if(callback) callback(true, data.items);
            } else {
                if(callback) callback(false);
            }
        });
    }
    
    function loadTasks(callback) {
        var batch = new gapi.Batch();
        var currentList = me.getCurrentList();
        for(var id in data) {
            if(currentList.id == me.ALL_LIST.id || id == currentList.id) {
                batch.add(id, new gapi.Request({
                    path: '/tasks/v1/lists/' + id + '/tasks',
                    method: 'GET',
                    params: {
                        showCompleted: 'false',
                        fields: 'items(due,id,notes,parent,position,title)'
                    }
                }));
            }
        }
        
        batch.execute(function(responses) {
            var tasks = [];
            for(var id in responses) {
                if(responses[id].success && responses[id].data.items) {
                    var list = {
                        id: id,
                        title: data[id].title
                    };
                    
                    for(var i = 0, item; item = responses[id].data.items[i]; i++) {
                        tasks.push(new Task(item, list));
                    }
                }
            }
            
            if(callback) callback(tasks);
        });
    }
    
    return me;
})();