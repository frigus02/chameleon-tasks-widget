<?php

// Requires the Google APIs Client Library for PHP
// https://code.google.com/p/google-api-php-client/
require_once '../../oauth2/apiClient.php';

$client = new apiClient();
$client->setClientId('<your client id>');
$client->setClientSecret('<your client secret>');
$client->setRedirectUri('http://kuehle.me/chameleon/tasks/services/oauth2.php');
$client->setAccessType('offline');
$client->setScopes('https://www.googleapis.com/auth/tasks https://www.googleapis.com/auth/userinfo.email');
$client->setApprovalPrompt('force');

if(isset($_GET['code'])) {
    $client->authenticate();
    $token = json_decode($client->getAccessToken());
    header('Location: http://chameleon.teknision.com/widgets/common/oauth/callback/?' . http_build_query($token));
} elseif(isset($_GET['refresh_token'])) {
    $client->refreshToken($_GET['refresh_token']);
    header('Content-type: application/json');
    echo $client->getAccessToken();
} else {
    echo $client->createAuthUrl();
}

?>